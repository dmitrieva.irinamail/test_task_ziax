# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api
from resources import Sentence


def create_app() -> Flask:
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(Sentence, "/sentence")
    return app


if __name__ == "__main__":
    main_app = create_app()
    main_app.run()
