def test_auth_key(client, sentence_entity) -> None:
    """Проверка верной отработки при корректном хэдере авторизации."""
    resp = client.post("/sentence", json={"sentence": sentence_entity.sentence})
    assert resp.status_code == 201


def test_auth_key_wrong(app) -> None:
    """Проверка верной отработки при некорректном хэдере авторизации."""
    client = app.test_client()
    resp = client.post("/sentence")
    assert resp.status_code == 401
    assert resp.json == {
        "status": "error",
        "error_type": "Unauthorized",
        "error_message": "Request is not authorized",
    }
