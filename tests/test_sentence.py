from random import randint


def test_sentence_json(client, sentence_entity) -> None:
    """Проверка корректности json-ответа сервера."""
    resp = client.post("/sentence", json={"sentence": sentence_entity.sentence})
    assert resp.status_code == 201
    assert resp.json == {
        "status": "ok",
        "first_declined_word": sentence_entity.first_declined_word(),
        "last_norm_form": sentence_entity.last_norm_form(),
        "num_words": sentence_entity.num_words(),
    }


def test_sentence_cyrillic(client) -> None:
    """Проверка, что система принимает только кириллицу."""
    resp = client.post("/sentence", json={"sentence": "wrong 儒家"})
    assert resp.status_code == 400
    assert resp.json == {
        "status": "error",
        "error_type": "ValidationError",
        "error_message": {"sentence": ["String does not match expected pattern."]},
    }


def test_sentence_first_digit(client, sentence_entity) -> None:
    """Проверка, что в качестве первого слова может быть число."""
    sentence_entity.sentence = f"{randint(1, 100)} {sentence_entity.sentence}"
    resp = client.post("/sentence", json={"sentence": sentence_entity.sentence})
    assert resp.status_code == 201
    assert resp.json == {
        "status": "ok",
        "first_declined_word": sentence_entity.first_declined_word(),
        "last_norm_form": sentence_entity.last_norm_form(),
        "num_words": sentence_entity.num_words(),
    }


def test_sentence_empty(client) -> None:
    """Проверка, что если sentence пустой то в ответе должно быть сообщение об ошибке."""
    resp = client.post("/sentence", json={"sentence": ""})
    assert resp.status_code == 400
    assert resp.json == {
        "status": "error",
        "error_type": "ValidationError",
        "error_message": {"sentence": ["String does not match expected pattern."]},
    }
