import factory
from models import Sentence


class SentenceFactory(factory.Factory):
    class Meta:
        model = Sentence

    sentence = factory.Faker("sentence", locale="ru_RU")
