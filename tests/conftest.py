import pytest
from app import create_app
from flask import testing
from settings import AVAILABLE_KEYS
from werkzeug.datastructures import Headers

from tests.factories import SentenceFactory


class TestClient(testing.FlaskClient):
    def open(self, *args, **kwargs):
        api_key_headers = Headers([("key", AVAILABLE_KEYS[0])])
        headers = kwargs.pop("headers", Headers())
        headers.extend(api_key_headers)
        kwargs["headers"] = headers
        return super().open(*args, **kwargs)


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True

    with _app.app_context():
        yield _app


@pytest.fixture
def client(app):
    app.test_client_class = TestClient
    client = app.test_client()
    yield client


@pytest.fixture
def sentence_entity():
    entity = SentenceFactory.build()
    entity.sentence = entity.sentence.replace(".", "")
    return entity
