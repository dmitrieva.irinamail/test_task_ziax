from flask import request
from settings import AVAILABLE_KEYS


def auth_key_required(func):
    def wrapper(*args, **kwargs):
        key = request.headers.get("key")
        if key in AVAILABLE_KEYS:
            res = func(*args, **kwargs)
        else:
            res = {
                "status": "error",
                "error_type": "Unauthorized",
                "error_message": "Request is not authorized",
            }, 401
        return res

    return wrapper
