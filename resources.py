from flask import request
from flask_restful import Resource
from handlers import auth_key_required
from marshmallow import ValidationError
from schemas import SentenceSchema


class Sentence(Resource):
    @auth_key_required
    def post(self):
        data = request.json

        schema = SentenceSchema()
        try:
            sentence = schema.load(data)
        except ValidationError as exc:
            return {
                "status": "error",
                "error_type": "ValidationError",
                "error_message": exc.messages,
            }, 400
        return {
            "status": "ok",
            "first_declined_word": sentence.first_declined_word(),
            "last_norm_form": sentence.last_norm_form(),
            "num_words": sentence.num_words(),
        }, 201
