from typing import Dict

from marshmallow import Schema, fields, post_load, validate
from models import Sentence


class SentenceSchema(Schema):
    """Схема валидации входной фразы."""

    sentence = fields.String(
        required=True,
        allow_none=False,
        validate=validate.Regexp(r"^\d+$|^\d*[А-Яа-яЁё\s]+$"),
    )

    @post_load
    def create_entity(self, data: Dict, **kwargs) -> Sentence:
        return Sentence(**data)
