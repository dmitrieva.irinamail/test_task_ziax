from typing import List

import nltk
import pymorphy2

nltk.download('punkt')


class Sentence:
    def __init__(self, sentence: str):
        self._sentence = sentence
        self.morph = pymorphy2.MorphAnalyzer()
        self.words: list = nltk.word_tokenize(self._sentence, language="russian")

    @property
    def sentence(self) -> str:
        """Предложение."""
        return self._sentence

    @sentence.setter
    def sentence(self, new_sentence):
        """Заменяет предложение на новое, формирует актуальный список слов."""
        self._sentence = new_sentence
        self.words = nltk.word_tokenize(self._sentence, language="russian")

    def num_words(self) -> int:
        """Количество слов во фразе."""
        return len(self.words)

    def first_declined_word(self) -> List[str]:
        """Все склонения первого слова во фразе."""
        first_word = self.morph.parse(self.words[0])
        return list(map(lambda a: a.word, first_word[0].lexeme))

    def last_norm_form(self) -> List[str]:
        """Нормализованная форма последнего слова во фразе."""
        last_word = self.morph.parse(self.words[-1])
        return [last_word[0].normal_form]
