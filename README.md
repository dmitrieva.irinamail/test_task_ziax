## test-task-ziax

Стек технологий:

- python 3.9.14
- flask
- marshmallow
- pymorphy2
- nltk

Тесты:

- pytest
- factory-boy

# Для запуска:

1) Установить зависимости

```pip install -r requirements.txt```

2) Запустить файл app.py

```python app.py```